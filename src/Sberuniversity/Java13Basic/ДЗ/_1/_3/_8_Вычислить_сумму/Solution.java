package Sberuniversity.Java13Basic.ДЗ._1._3._8_Вычислить_сумму;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int p = input.nextInt();
        int b = 0;
        int[] a = new int[n];

        for (int i = 0; i < n; i++) {
            a[i] = input.nextInt();
        }

        for (int i = 0; i < n; i++) {
            if (a[i] > p) {
                b += a[i];
            }
        }
        System.out.println(b);
    }
}