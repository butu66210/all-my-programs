package Sberuniversity.Java13Basic.ДЗ._1._3._9_Количество_отрицательных_чисел;

/*

На вход последовательно подается возрастающая последовательность из n
целых чисел, которая может начинаться с отрицательного числа.
Посчитать и вывести на экран, какое количество отрицательных чисел было
введено в начале последовательности. Помимо этого нужно прекратить
выполнение цикла при получении первого неотрицательного числа на вход.
Ограничения:
0 < n < 1000
-1000 < ai < 1000
Пример:
Входные данные
-55 -42 -19 -15 17 33 4

Выходные данные
10 20 0

 */




import java.util.ArrayList;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        ArrayList<Integer> mylist = new ArrayList<Integer>();
        while (scanner.hasNextInt()) {

            int i = scanner.nextInt();
            if (i >= 0) {
                System.out.print(mylist.size());
                break;
            }
            mylist.add(i);
        }
        }
    }