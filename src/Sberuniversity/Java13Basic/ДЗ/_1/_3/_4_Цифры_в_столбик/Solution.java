package Sberuniversity.Java13Basic.ДЗ._1._3._4_Цифры_в_столбик;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int n = input.nextInt();
        String sb = Integer.toString(n);

        for (int i = 0; i < sb.length(); i++) {
            System.out.println(sb.charAt(i));
        }
    }
}