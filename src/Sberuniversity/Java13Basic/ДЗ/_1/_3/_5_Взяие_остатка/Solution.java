package Sberuniversity.Java13Basic.ДЗ._1._3._5_Взяие_остатка;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int m = input.nextInt();
        int n = input.nextInt();

        while (m >= n) {
            m = m - n;
        }
        System.out.print(m);
    }
}