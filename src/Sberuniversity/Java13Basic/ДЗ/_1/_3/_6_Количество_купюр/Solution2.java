package Sberuniversity.Java13Basic.ДЗ._1._3._6_Количество_купюр;

import java.util.Scanner;

public class Solution2 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int n = input.nextInt();
        int m = 8;

        if (n < m)
            System.out.print(0 + " ");
        else {
            System.out.print(n / m + " ");
            n = n % m;
        }

        m = 4;

        if (n < m)
            System.out.print(0 + " ");
        else {
            System.out.print(n / m + " ");
            n = n % m;
        }

        m = 2;

        if (n < m)
            System.out.print(0 + " ");
        else {
            System.out.print(n / m + " ");
            n = n % m;
        }


        m = 1;

        if (n < m)
            System.out.print(0);
        else {
            System.out.print(n / m);
            n = n % m;
        }
    }
}