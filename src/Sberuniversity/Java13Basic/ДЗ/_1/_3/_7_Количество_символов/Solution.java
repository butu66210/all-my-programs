package Sberuniversity.Java13Basic.ДЗ._1._3._7_Количество_символов;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        String s = input.nextLine();

        s = s.replaceAll("\\s+","");

        int count = 0;

        for (int i = 0; i < s.length(); i++) {
            count++;
        }
        System.out.println(count);

    }
}