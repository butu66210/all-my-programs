package Sberuniversity.Java13Basic.ДЗ._1._3._2_Сумма_чисел;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int m = input.nextInt();
        int n = input.nextInt();

        long summ = 0;

        for (int i = m; i <= n; i++) {
            summ +=i;
        }
        System.out.print(summ);
    }
}