package Sberuniversity.Java13Basic.ДЗ._1._3._3_Вычислить_выражение;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int m = input.nextInt();
        int n = input.nextInt();

        long summ = 0;
        int step = 1;

        for (int i = m; step <= n; step++) {
            summ += (Math.pow(i, step));
        }
        System.out.print(summ);
    }
}