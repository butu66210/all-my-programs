package Sberuniversity.Java13Basic.ДЗ._1._2._13_Проверить_посылки;


import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        String mailPackage = input.nextLine();
        String s2 = ".{0,}";
        String s3 = "камни";
        String s4 = "запрещенная продукция";


        if (mailPackage.contains(s3) && mailPackage.contains(s4))
            System.out.println("в посылке камни и запрещенная продукция");
        else if (mailPackage.contains(s3))
            System.out.println("камни в посылке");
        else if (mailPackage.contains(s4))
            System.out.println("в посылке запрещенная продукция");
        else if (mailPackage.matches(s2))
            System.out.println("все ок");
        }
    }
