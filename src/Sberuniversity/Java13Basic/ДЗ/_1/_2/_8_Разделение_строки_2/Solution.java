package Sberuniversity.Java13Basic.ДЗ._1._2._8_Разделение_строки_2;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {

        String s1;

        Scanner input = new Scanner(System.in);

        s1 = input.nextLine();

        String lastWord = s1.substring(s1.lastIndexOf(" ")+1);

        String newStr = s1.replaceAll(lastWord, "");

        System.out.println(newStr.trim());
        System.out.println(lastWord);

    }
}