package Sberuniversity.Java13Basic.ДЗ._1._2._12_Проверить_пароль;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        String s1 = input.nextLine();
        String s2 = "(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[_*-])(?=\\S+$).{8,}";

        if (s1.matches(s2))
            System.out.println("пароль надежный");
        else
            System.out.println("пароль не прошел проверку");
    }
}