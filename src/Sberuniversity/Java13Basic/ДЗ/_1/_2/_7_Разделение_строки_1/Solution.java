package Sberuniversity.Java13Basic.ДЗ._1._2._7_Разделение_строки_1;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {

        String s1;
        String s2;

        Scanner input = new Scanner(System.in);

        s1 = input.next();
        s2 = input.nextLine();


        System.out.println(s1);
        System.out.println(s2.trim());
    }
}