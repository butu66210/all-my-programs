package Sberuniversity.Java13Basic.ДЗ._1._2._14_Покупка_телефона;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {

        int topPrise = 120000;
        int downPrise = 50000;
        String samsung = "samsung";
        String iphone = "iphone";

        Scanner input = new Scanner(System.in);

        String model = input.nextLine();

        int price = input.nextInt();


        if (downPrise <= price && price<= topPrise && model.contains(samsung))
            System.out.println("Можно купить");
        else if (downPrise <= price && price<= topPrise && model.contains(iphone))
            System.out.println("Можно купить");
        else
            System.out.println("Не подходит");
    }
}
