package Sberuniversity.Java13Basic.ДЗ._1._2._9_Тригонометрическое_тождество;

import java.util.Scanner;

    public class Solution {
        public static void main(String[] args) {

            Scanner scan = new Scanner(System.in);
            double a = scan.nextDouble();

                double b = Math.pow(Math.sin(a), 2);
                double c = Math.pow(Math.cos(a), 2);
                System.out.println( (int)((b + c) + 0.1) == 1 );

        }
    }