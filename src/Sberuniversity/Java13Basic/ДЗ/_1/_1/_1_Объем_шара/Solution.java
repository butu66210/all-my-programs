package Sberuniversity.Java13Basic.ДЗ._1._1._1_Объем_шара;

import java.util.Scanner;

public class Solution {
    public static void main (String [] args) {
        Scanner scan = new Scanner(System.in);
        int r = scan.nextInt();
        double v = 4 / 3.0 * Math.PI * Math.pow(r, 3);
		System.out.println(v);
    }
}
