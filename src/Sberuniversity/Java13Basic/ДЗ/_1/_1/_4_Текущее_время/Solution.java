package Sberuniversity.Java13Basic.ДЗ._1._1._4_Текущее_время;

import java.util.Scanner;

public class Solution {
    public static void main (String [] args) {
        Scanner scan = new Scanner(System.in);
        int count = scan.nextInt();
        int hours = count / 3600;
		int minutes = count % 3600 / 60;
		System.out.println(hours + " " + minutes);
    }
}
