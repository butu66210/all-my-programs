package Sberuniversity.Java13Basic.ДЗ._1._1._7_Перестановка_цифр;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        String s = Integer.toString(n);
        if (s.length() > 1) s = s.substring(s.length() - 1, s.length()) + s.substring (1, s.length()-1) + s.substring(0,1);
        n = Integer.parseInt(s);
        System.out.println(s);
    }
}

// На вход подается двузначное число n. Выведите число, полученное
// перестановкой цифр в исходном числе n. Если после перестановки получается
// ведущий 0, его также надо вывести.