package Sberuniversity.Java13Basic.ДЗ._2._1._4_Количество_различных_элементов;

import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int N = input.nextInt();
        int[] a = new int[N];

        for (int i = 0; i < N; i++) {
            a[i] = input.nextInt();
        }

        for (int j = 0; j < N; j++) {
            if (j < a.length - 2) {
                if (a[j] == a[j + 1] & a[j] == a[j + 2]) {
                    System.out.println(3 + " " + a[j]);
                    j += 2;
                } else if (a[j] == a[j + 1]) {
                    System.out.println(2 + " " + a[j]);
                    j += 1;
                } else {
                    System.out.println(1 + " " + a[j]);
                }
            } else if (j < a.length - 1) {
                if ( a[j] == a[j + 1]) {
                    System.out.println(2 + " " + a[j]);
                    j += 1;
                } else {
                    System.out.println(1 + " " + a[j]);
                }
            } else if (j == a.length - 1) {
                System.out.println(1 + " " + a[j]);
            }
    }
}}