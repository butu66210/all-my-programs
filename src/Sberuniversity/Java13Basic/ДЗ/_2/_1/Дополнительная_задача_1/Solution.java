package Sberuniversity.Java13Basic.ДЗ._2._1.Дополнительная_задача_1;

/*

1. (2 балла) Создать программу генерирующую пароль.
На вход подается число N — длина желаемого пароля. Необходимо проверить,
что N >= 8, иначе вывести на экран "Пароль с N количеством символов
небезопасен" (подставить вместо N число) и предложить пользователю еще раз
ввести число N.
Если N >= 8 то сгенерировать пароль, удовлетворяющий условиям ниже и
вывести его на экран. В пароле должны быть:
● заглавные латинские символы
● строчные латинские символы
● числа
● специальные знаки(_*-)

 */

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Solution {
    public static void main(String[] args) {

        final String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        Scanner scan = new Scanner(System.in);
        int N = scan.nextInt();//Вводим длину пароля

        while (N < 8) { //Проверяем длину пароля
            System.out.println("Пароль с " + N + " количеством символов небезопасен. " +
                    "Пожалуйста, введите длину пароля еще раз: ");
            N = scan.nextInt();
        }

        String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";//Строчные латинские символы
        String CHAR_UPPER = CHAR_LOWER.toUpperCase();//Заглавные латинские символы
        String NUMBER = "0123456789";//Числа
        String OTHER_CHAR = "*_-";//Специальные символы


        for (int i = 0; i < 3; i++) {
            System.out.print(CHAR_LOWER.charAt(ThreadLocalRandom.current().nextInt(0, CHAR_LOWER.length())));
        }

        for (int i = 0; i < 2; i++) {
            System.out.print(OTHER_CHAR.charAt(ThreadLocalRandom.current().nextInt(0, OTHER_CHAR.length())));
        }

        for (int i = 0; i < 1; i++) {
            System.out.print(CHAR_UPPER.charAt(ThreadLocalRandom.current().nextInt(0, CHAR_UPPER.length())));
        }

        for (int i = 0; i < N - 6; i++) {
            System.out.print(NUMBER.charAt(ThreadLocalRandom.current().nextInt(0, NUMBER.length())));
        }

    }


}