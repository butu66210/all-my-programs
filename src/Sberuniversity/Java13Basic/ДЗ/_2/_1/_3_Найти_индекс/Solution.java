package Sberuniversity.Java13Basic.ДЗ._2._1._3_Найти_индекс;

import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int N = input.nextInt();
        int[] a = new int[N];

        for (int i = 0; i < N; i++) {
            a[i] = input.nextInt();
        }

        int X = input.nextInt();

       int b =  java.util.Arrays.binarySearch(a, X);
       if (b>=0){
            System.out.println(b + 2);
        } else {
           System.out.println(-b - 1);
       }
    }
}