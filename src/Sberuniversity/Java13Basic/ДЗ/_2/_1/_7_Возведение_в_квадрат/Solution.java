package Sberuniversity.Java13Basic.ДЗ._2._1._7_Возведение_в_квадрат;

import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int N = input.nextInt();
        int[] a = new int[N];//ОБЪЯВЛЯЕМ МАССИВ

        for (int i = 0; i < N; i++) { //ВВОДИМ МАССИВ
            a[i] = input.nextInt();
        }

        int[] m = new int[N];

        for (int i = 0; i < N; i++) {
            m[i] = (int) Math.pow(a[i], 2);
        }

        java.util.Arrays.parallelSort(m);


        for (int i = 0; i<N; i++) { //ВЫВОДИМ МАССИВ НА ЭКРАН
            System.out.print(m[i] + " ");
        }
    }
}