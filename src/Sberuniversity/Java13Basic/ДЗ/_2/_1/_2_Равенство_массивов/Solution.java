package Sberuniversity.Java13Basic.ДЗ._2._1._2_Равенство_массивов;

import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int N = input.nextInt();
        int[] a = new int[N];

        for (int i = 0; i < N; i++) {
            a[i] = input.nextInt();
        }

        int M = input.nextInt();
        int[] b = new int[M];

        for (int j = 0; j < M; j++) {
            b[j] = input.nextInt();
        }

        System.out.println(java.util.Arrays.equals(a, b));

    }
}