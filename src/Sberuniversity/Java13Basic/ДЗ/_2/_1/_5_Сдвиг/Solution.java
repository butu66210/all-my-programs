package Sberuniversity.Java13Basic.ДЗ._2._1._5_Сдвиг;

import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int N = input.nextInt(); //ВВОДИМ КОЛИЧЕСТВО ЭЛЕМЕНТОВ МАССИВА
        int[] a = new int[N];


        for (int i = 0; i < N; i++) { //ВВОДИМ МАССИВ
            a[i] = input.nextInt();
        }


        int M = input.nextInt(); //ВВОДИМ ВЕЛИЧИНУ СДВИГА ВПРАВО


        int[] b = new int[N];//СОЗДАЕМ ПУСТОЙ МАССИВ

        for (int i = 0; i < N; i++) { //ЦИКЛ РАБОТАЕТ ПОКА КАЖДЫЙ ЭЛЕМЕНТ МАССИВА ЕГО НЕ ПРОЙДЕТ

            if (N - i <= M) {//ЕСЛИ ДЛИНА МАССИВА МИНУС ИНДЕКС ЭЛЕМЕНТА МЕНЬШЕ И РАВНО ДЛИНЕ СДВИГА

               b[-(N - i - M)]  = a[i];

//                System.out.print(b[M-1]+ " "); //ПОКАЗЫВАЕТ ЧТО СЕЙЧАС В ЭТОЙ ЯЧЕЙКЕ
//                System.out.println(a[i]+ " ");//ПОКАЗЫВАЕТ ЧТО СЕЙЧАС В ЭТОЙ ЯЧЕЙКЕ

            } else if (N - i > M) {

                 b[i+M] = a[i];

//                System.out.print(b[i+M]+ " ");//ПОКАЗЫВАЕТ
//                System.out.println(a[i]+ " ");//ПОКАЗЫВАЕТ

            } else if (M <= 0) {
                break;
            }

        }


        // ВЫВОДИТ НА ЭКРАН РЕЗУЛЬТАТ СДВИГА
        for (int i = 0; i < N; i++) {
            System.out.print(b[i]+ " ");
        }

    }
}