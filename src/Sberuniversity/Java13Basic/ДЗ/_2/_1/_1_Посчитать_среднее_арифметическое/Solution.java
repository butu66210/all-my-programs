package Sberuniversity.Java13Basic.ДЗ._2._1._1_Посчитать_среднее_арифметическое;

import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int N = input.nextInt();
        double[] a = new double[N];

        for (int i = 0; i < N; i++) {
            a[i] = input.nextDouble();
        }

        double b = 0;

        for (int i=0; i < N; i++) {
                b += a[i];
        }

        double c = b/N;

        System.out.print(c);

    }
}