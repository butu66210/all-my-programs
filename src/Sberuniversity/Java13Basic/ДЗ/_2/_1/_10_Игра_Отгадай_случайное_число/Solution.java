package Sberuniversity.Java13Basic.ДЗ._2._1._10_Игра_Отгадай_случайное_число;

import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {

    gameZagadkaChisel();

    }

        public static void gameZagadkaChisel () {
        Scanner input = new Scanner(System.in);
        double zagadka = (Math.random() * 1000); //ГЕНЕРАЦИЯ РАНДОМНОЙ ЦИФРЫ ТИПА DOUBLE

        int M = (int) zagadka; //ПРИВОДИМ РАНДОМНОЕ ЧИСЛО К ТИПУ INT
//        System.out.println(M); //ВЫВОД ДЛЯ ПРОВЕРКИ РАБОТОСПОСОБНОСТИ КОДА

        System.out.println("Угадайте загаданное целое число от 0 до 1000 включительно.");

        int otgadka = 1001;//ПРИСВАЕВАЕМ ПЕРЕМЕННОЙ ТАКОЕ ЗНАЧЕНИЕ, КОТОРОЕ НИКОГДА НЕ РАВНО ЗАГАДКЕ

        while (M != otgadka &  otgadka >= 0) {
            otgadka = input.nextInt();//ВВОД ОТГАДКУ
            if (M == otgadka) { //ЕСЛИ ПРАВИЛЬНО ОТГАДАЛ
                System.out.print("Победа!");
                break;

            } else if (otgadka < 0) {//ЕСЛИ ОТГАДКА МЕНЬШЕ НУЛЯ, СРАЗУ ЗАВЕРШИТЬ БЕЗ ВЫВОДА СООБЩЕНИЯ
                break;

            } else if (otgadka < M) { //ЕСЛИ ОТГАДКА МЕНЬШЕ ЗАГАДКИ
                System.out.println("Это число меньше загаданного.");

            } else if (otgadka > M) {//ЕСЛИ ОТГАДКА БОЛЬШЕ ЗАГАДКИ
                System.out.println("Это число больше загаданного.");

            }
        }
    }
}