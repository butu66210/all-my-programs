package Sberuniversity.Java13Basic.ДЗ._2._1._8_Найти_ближайшее;

import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int N = input.nextInt();
        int[] a = new int[N];//ОБЪЯВЛЯЕМ МАССИВ

        for (int i = 0; i < N; i++) { //ВВОДИМ МАССИВ
            a[i] = input.nextInt();
        }

        java.util.Arrays.parallelSort(a);

        int M = input.nextInt(); //ВВОДИМ ЧИСЛО ДЛЯ КОТОРОГО БУДЕМ ИСКАТЬ НАИБОЛЕЕ ПОХОЖЕЕ

        int b =  java.util.Arrays.binarySearch(a, M+1);
        if (b>0) {
            System.out.print(a[b]);
        } else {
          int c =  java.util.Arrays.binarySearch(a, M-1);
            System.out.print(a[c]);
        }
    }
}