package Sberuniversity.Java13Basic.ДЗ._2._2._7_Конкурс_красоты;

import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int N = scan.nextInt(); //Количество участников конкурса


        String [] bosses = new String[N]; //Объявляем массив хозяев

        for (int i = 0; i < N; i++) {  //Вводим хозяев
            bosses[i] = scan.next();
        }


//        for (int i = 0; i < N; i++){             //Проверяем массив хозяев
//            System.out.println(bosses[i]);
//        }

        String [] doges = new String[N]; //Объявляем массив собак

        for (int i = 0; i < N; i++) {  //Вводим собак
            doges[i] = scan.next();
        }
        System.out.print("");

//        for (int i = 0; i < N; i++){             //Проверяем массив собак
//                System.out.println(doges[i]);
//            }


        int [][] ocenki = new int[N][3]; //Объявляем массив оценок



        for (int i = 0; i < N; i++) {  //Вводим оценки
            for (int j = 0; j < 3; j++) {
                ocenki[i][j] = scan.nextInt();
            }
        }

//        for (int i = 0; i < N; i++){             //Проверяем массив оценок
//                for (int j = 0; j < 3; j++) {
//                    System.out.println(ocenki[i][j]);
//                }
//        }

        double [] srednee = new double[N];//Создаем массив для средних оценок

        double w = 0; //Сумма строки

        for (int i = 0; i < N; i++) { //Считаем средние оценки
            for (int j = 0; j < 3; j++) {
                w += ocenki[i][j];
            }
            srednee[i] = ((double)((int)((w/3)*10)))/10 ; //Оставляем один знак после запятой
            w = 0;
        }

//        for (int j = 0; j < N; j++) {  //Проверяем массив средних оценок
//            System.out.println(srednee[j] + " ");
//        }

        //Результаты в массиве: 0 - Жучка,     1 - Кнопка,  2 - Цезарь, 3 - Добряш.
//                                                      Иван,         Николай,            Анна,         Дарья


        int k = 0;
        int V = N;

        for (int r = 0; r < 3; r++) {
            for (int j = 0; j < N; j++) {                 //ИЩЕМ 1, 2, 3 МЕСТО
                for (int i = 0; i < N; i++) {
                    if (srednee[j] >= srednee[i]) {
                        k = k+1;
                    }
                }
                if (k == V) {
                    System.out.println(bosses[j] + ": " + doges[j] + ", " + srednee[j]);
                    break;
                } else {
                    k = 0;
                }
            }
            V--;
            k = 0;
        }
    }
}