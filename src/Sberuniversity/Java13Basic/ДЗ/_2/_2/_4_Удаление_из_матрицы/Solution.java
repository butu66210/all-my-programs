package Sberuniversity.Java13Basic.ДЗ._2._2._4_Удаление_из_матрицы;

import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int N = input.nextInt(); //ВВОДИМ КОЛИЧЕСТВО СТОЛБЦОВ И СТРОК ДВУМЕРНОГО МАССИВА

        int[][] a = new int[N][N]; //ОБЪЯВЛЯЕМ ДВУМЕРНЫЙ МАССИВ СТРОК

        for (int j = 0; j < N; j++) {  //ВВОДИМ САМ ДВУМЕРНЫЙ МАССИВ
            for (int i = 0; i < N; i++) {
                a[j][i] = input.nextInt();
            }
        }

        int P = input.nextInt(); //Число, равное которому нужно будет удалить из массива вместе со всем
        // столбцом и строкой с уменьшением самого массива

        int c = 0; //строка
        int d = 0;//столбец

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (a[i][j] == P) {
                    c = i; //строка
                    d = j;//столбец
                }
            }
        }

        //Перемещаем элементы массива
        for (int i = 0; i < N-1; i++) {
            for (int j = d; j < N-1; j++) {
                a[i][j] = a[i][j+1];
            }
        }


//        for (int j = 0; j < N; j++) {  //ВЫВОДИМ ДВУМЕРНЫЙ МАССИВ НА ЭКРАН
//
//            for (int i = 0; i < N; i++) {
//                System.out.print(a[j][i]);
//
//                if (i < N-1) {
//                    System.out.print(" ");
//                }
//            }
//            System.out.println();
//        }


        for (int i = c; i < N-1; i++) {
            for (int j = 0; j < N-1; j++) {
                a[i][j] = a[i-1][j];
            }
        }

//        for (int j = 0; j < N; j++) {  //ВЫВОДИМ ДВУМЕРНЫЙ МАССИВ НА ЭКРАН
//
//            for (int i = 0; i < N; i++) {
//                System.out.print(a[j][i]);
//
//                if (i < N-1) {
//                    System.out.print(" ");
//                }
//            }
//            System.out.println();
//        }


        int[][] k = new int[N - 1][N - 1]; //Новый массив

        for (int i = 0; i < N-1; i++) {//Копируем старый массив в новый без нижней строки и самого правого столбца
            for (int j = 0; j < N - 1; j++) {
                    k[i][j] = a[i][j];
            }
        }



//        System.out.println ("Результат");
        for (int j = 0; j < N - 1; j++) {  //ВЫВОДИМ ДВУМЕРНЫЙ МАССИВ НА ЭКРАН

            for (int i = 0; i < N - 1; i++) {
                System.out.print(k[j][i]);

                if (i < N - 2) {
                System.out.print(" ");
                }
            }
            System.out.println();
            }
    }
}