package Sberuniversity.Java13Basic.ДЗ._2._2._1_Минимальный_элемент;

import java.util.Arrays;
import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int N = input.nextInt(); //ВВОДИМ КОЛИЧЕСТВО СТОЛБЦОВ ДВУМЕРНОГО МАССИВА
        int M = input.nextInt(); //ВВОДИМ КОЛИЧЕСТВО СТРОК ДВУМЕРНОГО МАССИВА

        int[][] a = new int[M][N];

        for (int j = 0; j < M; j++) {  //ВВОДИМ САМ ДВУМЕРНЫЙ МАССИВ
            for (int i = 0; i < N; i++) {
                a[j][i] = input.nextInt();
            }
        }

        for (int i = 0; i < a.length; i++) {
            Arrays.sort(a[i]); //СОРТИРОВКА МАССИВА ПО ВОЗРАСТАНИЮ В КАЖДОЙ СТРОКЕ
        }

        int[] b = new int[M];

//        for (int j = 0; j < M; j++) {  //ВЫВОДИМ ДВУМЕРНЫЙ МАССИВ НА ЭКРАН ПОСЛЕ СОРТИРОВКИ
//            System.out.println(" ");
//            for (int i = 0; i < N; i++) {
//                System.out.print(a[j][i] + " ");
//            }
//        }

        for (int i = 0; i < M; i++) { //ПРИСВАИВАЕМ ОДНОМЕРНОМУ МАССИВУ ЗНАЧЕНИЯ ПЕРВОГО ЭЛЕМЕНТА КАЖДОЙ СТРОКИ ДВУМЕРНОГО МАССИВА
            b[i] = a[i][0];
        }

            for (int i = 0; i < M; i++) { //ВЫВОДИМ ОДНОМЕРНЫЙ МАССИВ НА ЭКРАН
                System.out.print(b[i] + " ");
                }
            }
        }