package Sberuniversity.Java13Basic.ДЗ._2._2._3_Шахматы;

/*
		Y - строка
		Х - столбец
		Заменяем на Х
		[Y - 2][X - 1], [Y - 2][X + 1]
		[Y - 1][X - 2], [Y - 1][X + 2]
		[Y + 2][X - 1], [Y + 2][X + 1]
		[Y + 1][X - 2], [Y + 1][X + 2]
*/

import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int N = input.nextInt(); //ВВОДИМ КОЛИЧЕСТВО СТОЛБЦОВ И СТРОК ДВУМЕРНОГО МАССИВА

        String[][] a = new String[N][N]; //ОБЪЯВЛЯЕМ ДВУМЕРНЫЙ МАССИВ СТРОК

        for (int j = 0; j < N; j++) {  // ПРИСВАИВАЕМ ЗНАЧЕНИЕ 0 ВСЕМ ЕГО ЭЛЕМЕНТАМ
            for (int i = 0; i < N; i++) {
                a[j][i] = "0";
            }
        }

        int X = input.nextInt(); //СТОЛБЕЦ
        int Y = input.nextInt(); //СТРОКА

        a[Y][X] = "K";

        if (Y - 2 >= 0 & X - 2 >= 0 & Y +2 <= N & X + 2 <= N) {
            a[Y + 2][X - 1] = a[Y + 2][X + 1] =
            a[Y + 1][X - 2] = a[Y + 1][X + 2] =
            a[Y - 1][X - 2] = a[Y - 1][X + 2] =
            a[Y - 2][X - 1]= a[Y - 2][X + 1] = "X";

        } else if (Y + 2 > N) {
            a[Y - 1][X + 2] = a[Y - 2][X + 1]  = "X";
        }


        for (int j = 0; j < N; j++) {  //ВЫВОДИМ ДВУМЕРНЫЙ МАССИВ НА ЭКРАН

            for (int i = 0; i < N; i++) {
                System.out.print(a[j][i]);
                if (i < N-1) {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}