package Sberuniversity.Java13Basic.ДЗ._2._2._10_Обратный_порядок;

import java.util.Scanner;

public class Solution {

public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int N = scan.nextInt(); //Количество участников конкурса
    xMethod(N);
}

public static void xMethod(int n) {
        if (n > 0) {
            System.out.print(n % 10 + " ");
            xMethod(n / 10);
        }
    }
}