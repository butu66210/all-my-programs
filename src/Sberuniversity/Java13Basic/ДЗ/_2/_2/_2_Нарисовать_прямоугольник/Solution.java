package Sberuniversity.Java13Basic.ДЗ._2._2._2_Нарисовать_прямоугольник;

/*
	0 1 2 3 4 5 6 7 = N

0 	0 0 0 0 0 0 0 0
1 	0 0 0 0 0 0 0 0
2 	0 1 1 1 0 0 0 0
3 	0 1 0 0 0 0 0 0
4 	0 1 1 1 0 0 0 0
5 	0 0 0 0 0 0 0 0
6 	0 0 0 0 0 0 0 0
7 	0 0 0 0 0 0 0 0

=
N

координаты: столбец X - 1, строка Y - 2                				 
		    столбец J - 3, строка Z - 4
			
В строке Y (от столбца X до столбца j) меняем 0 на 1 пока номер столбца от исходного X не станет больше номера столбца j.

В строке Z (от столбца X до столбца j) меняем 0 на 1 пока номер столбца от исходного Х не станет больше номера столбца j.

В столбце X (от строк Y до Z) меняем 0 на 1 пока номер строки от исходной Y не станет больше номера строки Z.

В столбце J (от строк Y до Z) меняем 0 на 1 пока номер строки от исходной Y не станет больше номера строки Z.

Выводим двумерный массив.
*/


import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int N = input.nextInt(); //ВВОДИМ КОЛИЧЕСТВО СТОЛБЦОВ И СТРОК ДВУМЕРНОГО МАССИВА

        int[][] a = new int[N][N]; //ОБЪЯВЛЯЕМ ДВУМЕРНЫЙ МАССИВ

        for (int j = 0; j < N; j++) {  // ПРИСВАИВАЕМ ЗНАЧЕНИЕ 0 ВСЕМ ЕГО ЭЛЕМЕНТАМ
            for (int i = 0; i < N; i++) {
                a[j][i] = 0;
            }
        }

        int X = input.nextInt(); //КООРДИНАТЫ ВЕРХНЕГО-ЛЕВОГО НАЧАЛА ПРЯМОУГОЛЬНИКА: СТОЛБЕЦ
        int Y = input.nextInt(); //СТРОКА

        int J = input.nextInt(); //КООРДИНАТЫ НИЖНЕГО-ПРАВОГО ОКОНЧАНИЯ ПРЯМОУГОЛЬНИКА: СТОЛБЕЦ
        int Z = input.nextInt(); //СТРОКА

        for (int i = X; i <= J; i++) { //В строке Y (от столбца X до столбца j) меняем 0 на 1 пока номер столбца от исходного X не станет больше номера столбца j.
            a[Y][i] = 1;
        }

        for (int i = X; i <= J; i++) { //В строке Z (от столбца X до столбца j) меняем 0 на 1 пока номер столбца от исходного Х не станет больше номера столбца j.
            a[Z][i] = 1;
        }

        for (int i = Y; i <= Z; i++) {//В столбце X (от строк Y до Z) меняем 0 на 1 пока номер строки от исходной Y не станет больше номера строки Z.
            a[i][X] = 1;
        }

        for (int i = Y; i <= Z; i++) {//В столбце J (от строк Y до Z) меняем 0 на 1 пока номер строки от исходной Y не станет больше номера строки Z.
            a[i][J] = 1;
        }

        for (int j = 0; j < N; j++) {  //ВЫВОДИМ ДВУМЕРНЫЙ МАССИВ НА ЭКРАН

            for (int i = 0; i < N; i++) {
                System.out.print(a[j][i]);
                    if (i < N-1) {
                        System.out.print(" ");
                    }
            }
            System.out.println();
        }
    }
}