package Sberuniversity.Java13Basic.ДЗ._3._1._4_TimeUnit;

import java.util.regex.Pattern;

public class TimeValidator {//Средство проверки времени

    public static boolean validateHours(Integer hours) {
        if (hours >= 0 & hours < 24) {
            return true;
        } else {
            System.out.print("Введено некорректное время");
            return false;
        }
    }

    public static boolean validateMinutesOrSeconds(Integer minutesOrSeconds) {
        if (minutesOrSeconds >= 0 & minutesOrSeconds < 60) {
            return true;
        } else {
            System.out.print("Введено некорректное время");
            return false;
        }
    }

}
