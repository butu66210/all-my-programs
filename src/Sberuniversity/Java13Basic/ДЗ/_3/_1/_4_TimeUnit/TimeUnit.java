package Sberuniversity.Java13Basic.ДЗ._3._1._4_TimeUnit;


import static Sberuniversity.Java13Basic.ДЗ._3._1._4_TimeUnit.TimeValidator.validateHours;
import static Sberuniversity.Java13Basic.ДЗ._3._1._4_TimeUnit.TimeValidator.validateMinutesOrSeconds;


public class TimeUnit {//Средство проверки времени

    private static Integer hours;          //Часы
    private static Integer minutes = 0;     //Минуты
    private static Integer seconds = 0;    //Секунды

    public static void TimeUnit(Integer newHours) {

        if (validateHours(newHours)) {
            hours = newHours;
        }
    }

    public static void TimeUnit(Integer newHours, Integer newMinutes) {

        if ((validateHours(newHours)) & validateMinutesOrSeconds(newMinutes)) {
            hours = newHours;
            minutes = newMinutes;
        }
    }

    public static void TimeUnit(Integer newHours, Integer newMinutes, Integer newSeconds) {

        if ((validateHours(newHours)) & validateMinutesOrSeconds(newMinutes) & validateMinutesOrSeconds(newSeconds)) {
            hours = newHours;
            minutes = newMinutes;
            seconds = newSeconds;

        }
    }

    public static void printTime() {
        System.out.print("\n");
        if (hours < 10 & minutes < 10 & seconds < 10) { //Добавляем недостающие нули и выводим на экран
            System.out.print("0" + hours + ":" + "0" + minutes + ":" + "0" + seconds);
        } else if (minutes < 10 & seconds < 10) {
            System.out.print(hours + ":" + "0" + minutes + ":" + "0" + seconds);
        } else if (hours < 10 & seconds < 10) {
            System.out.print("0" + hours + ":" + minutes + ":" + "0" + seconds);
        } else if (hours < 10 & minutes < 10) {
            System.out.print("0" + hours + ":" + "0" + minutes + ":" + seconds);
        } else if (hours < 10) {
            System.out.print("0" + hours + ":" + minutes + ":" + seconds);
        } else if (minutes < 10) {
            System.out.print(hours + ":" + "0" + minutes + ":" + seconds);
        } else if (seconds < 10) {
            System.out.print(hours + ":" + minutes + ":" + "0" + seconds);
        } else {
            System.out.print(hours + ":" + minutes + ":" + seconds);
        }
    }

    public static void printTimeAmPm() {
        if (hours <= 12) {
            printTime();
            System.out.println(" am");
        } else {
            hours -= 12;
            printTime();
            System.out.println(" pm");
        }
    }

    public static void pluseTime(Integer plusHours, Integer plusMinutes, Integer plusSeconds) {

        if ((validateHours(plusHours)) & validateMinutesOrSeconds(plusMinutes) & validateMinutesOrSeconds(plusSeconds)) {

            seconds += plusSeconds;
            if (seconds > 59) {
                seconds -= 60;
                minutes += 1;
            }

            minutes += plusMinutes;
            if (minutes > 59) {
                minutes -= 60;
                hours += 1;
            }

            hours += plusHours;
            if (hours > 24) {
                hours -= 24;
            }
        }
    }
}
