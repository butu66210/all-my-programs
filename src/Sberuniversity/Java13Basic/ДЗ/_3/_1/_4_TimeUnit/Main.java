package Sberuniversity.Java13Basic.ДЗ._3._1._4_TimeUnit;

/*

4. Необходимо реализовать класс TimeUnit с функционалом, описанным ниже
(необходимые поля продумать самостоятельно). Обязательно должны быть
реализованы валидации на входные параметры.
Конструкторы:
● Возможность создать TimeUnit, задав часы, минуты и секунды.
● Возможность создать TimeUnit, задав часы и минуты. Секунды тогда
должны проставиться нулевыми.
● Возможность создать TimeUnit, задав часы. Минуты и секунды тогда
должны проставиться нулевыми.
Публичные методы:
● Вывести на экран установленное в классе время в формате hh:mm:ss
● Вывести на экран установленное в классе время в 12-часовом формате
(используя hh:mm:ss am/pm)
● Метод, который прибавляет переданное время к установленному

 */


public class Main {

    public static void main(String[] args) {

    TimeUnit.TimeUnit(18); //Устанавливаем время
    TimeUnit.printTime();//Выводим в формате 24 часа
    TimeUnit.printTimeAmPm();//Выводим в формате до и после полудня
    TimeUnit.TimeUnit(17, 4);
    TimeUnit.printTime();
    TimeUnit.printTimeAmPm();
    TimeUnit.TimeUnit(3, 9,19);
    TimeUnit.printTime();
    TimeUnit.printTimeAmPm();

    TimeUnit.pluseTime(23, 54, 43);//Прибавляем указанное время
    TimeUnit.printTime();
    TimeUnit.printTimeAmPm();
    }
}
