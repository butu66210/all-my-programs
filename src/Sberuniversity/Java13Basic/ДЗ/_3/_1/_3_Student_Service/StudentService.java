package Sberuniversity.Java13Basic.ДЗ._3._1._3_Student_Service;


import Sberuniversity.Java13Basic.ДЗ._3._1._2_Student.Student;


import java.util.Arrays;
import java.util.Comparator;


public class StudentService {

    /**
     * Получаем иерархию студентов от большего среднего балла к меньшему.
     * Так интереснеее, чем получать просто лучшего.
     * При необходимости можно в StudentService исправить одну переменную и будет выводить только лучшего.
     * Если баллы у кого-то дублируются, то программа выводит только того из дублирующихся,
     * у кого наименьший индекс элемента массива.
     */
    public static void theBestStudent(Student[] students) {   //Ищем студента с лучшим средним баллом

        int k = 0;//Счетчик
        int V = students.length;
        int numberOfSeats = V; //Количество мест

        for (int r = 0; r < numberOfSeats; r++) {
            for (int j = 0; j < numberOfSeats; j++) {                 //ИЩЕМ 1, 2, 3 И ТАК ДАЛЕЕ МЕСТО
                for (int i = 0; i < numberOfSeats; i++) {
                    if (students[j].averageRating() >= students[i].averageRating()) {
                        k = k + 1;
                    }
                }
                if (k == V) {
                    System.out.println(students[j].getSurname() + " " + students[j].gatName() + ", " + students[j].averageRating());
                    break;
                } else {
                    k = 0;
                }
            }
            V--;
            k = 0;
        }

    }

    public static void sortBySurname(Student[] students) {

        Arrays.sort(students, Comparator.comparing(Student::getSurname)); // Сортировка по фамилии

        System.out.println("");

        for (int j = 0; j < students.length; j++) {
            System.out.println(students[j].getSurname() + " " + students[j].gatName() + ", " + students[j].averageRating());
        }

    }
}

