package Sberuniversity.Java13Basic.ДЗ._3._1._3_Student_Service;

/*

3. Необходимо реализовать класс StudentService.
У класса должны быть реализованы следующие публичные методы:
● bestStudent() — принимает массив студентов (класс Student из
предыдущего задания), возвращает лучшего студента (т.е. который
имеет самый высокий средний балл). Если таких несколько — вывести
любого.
● sortBySurname() — принимает массив студентов (класс Student из
предыдущего задания) и сортирует его по фамилии.

 */


import Sberuniversity.Java13Basic.ДЗ._3._1._2_Student.Student;


public class Main {
    public static void main(String[] args) {


        Student[] students = new Student[5];

        students[0] = new Student();
        students[0].setName("Юрий");
        students[0].setSurname("Бутузов");
        Integer[] studentGrades0 = new Integer[]{97, 100, 99, 93, 100};//Создаем оценки
        students[0].setGrades(studentGrades0); //Вводим оценки
//        System.out.println("Средний балл: "+ students[0].averageRating()+"\n");//Получаем средний балл


        students[1] = new Student();
        students[1].setName("Григорий");
        students[1].setSurname("Распутин");
        Integer[] studentGrades1 = new Integer[]{97, 100, 99, 93, 91};//Создаем оценки
        students[1].setGrades(studentGrades1); //Вводим оценки
//        System.out.println("Средний балл: "+ students[1].averageRating() +"\n");//Получаем средний балл


        students[2] = new Student();
        students[2].setName("Александр");
        students[2].setSurname("Невский");
        Integer[] studentGrades2 = new Integer[]{97, 100, 94, 93, 91};//Создаем оценки
        students[2].setGrades(studentGrades2); //Вводим оценки
//        System.out.println("Средний балл: "+ students[2].averageRating() +"\n");//Получаем средний балл

        students[3] = new Student();
        students[3].setName("Виктор");
        students[3].setSurname("Гюго");
        Integer[] studentGrades3 = new Integer[]{90, 100, 99, 93, 91};//Создаем оценки
        students[3].setGrades(studentGrades3); //Вводим оценки
//        System.out.println("Средний балл: "+ students[3].averageRating()+"\n");//Получаем средний балл

        students[4] = new Student();
        students[4].setName("Помелла");
        students[4].setSurname("Андерсен");
        Integer[] studentGrades4 = new Integer[]{97, 100, 88, 93, 91};//Создаем оценки
        students[4].setGrades(studentGrades4); //Вводим оценки
//        System.out.println("Средний балл: "+ students[4].averageRating()+"\n");//Получаем средний балл


        StudentService.theBestStudent(students); //Получаем иерархию по оценкам

        StudentService.sortBySurname(students);//Сортируем и выводим на экран

    }
}
