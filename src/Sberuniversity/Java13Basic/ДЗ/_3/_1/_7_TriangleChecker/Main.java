package Sberuniversity.Java13Basic.ДЗ._3._1._7_TriangleChecker;

/*
7. Реализовать класс TriangleChecker, статический метод которого принимает три
длины сторон треугольника и возвращает true, если возможно составить из них
треугольник, иначе false. Входные длины сторон треугольника — числа типа double.
Придумать и написать в методе main несколько тестов для проверки.
работоспособности класса (минимум один тест на результат true и один на результат false)

 */


import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        while (true) {// Бесконечный цикл
            System.out.println("Введите стороны треугольника: ");
            double a = scan.nextDouble();
            double b = scan.nextDouble();
            double c = scan.nextDouble();

            TriangleChecker.triangleChecking(a, b, c);

            //Тест  1 - скорпировать и ввести 5.4 7.6 9.3
            //Тест 2 - скопировать и ввести 78.45 32.67 12.16
        }
    }

}
