package Sberuniversity.Java13Basic.ДЗ._3._1._8_Atm;

public class Atm {

    private static int instance = 0; //Количество инстантов

    Atm() { //Счетчик инстантов
        instance++;
    }


    private static double rublesToDollars;
    private static double dollarsToRubles;

    private static double dollarsBill;
    private static double rublesBill;



    static void setMoneyCost(double rubToDol, double dolToRub) {
        if (rubToDol > 0 & dolToRub > 0) {
            rublesToDollars = rubToDol;
            dollarsToRubles = dolToRub;
        }
    }

    public static double rubToDollars(double rub) {
        System.out.print(" $ ");
        dollarsBill = (((int) ((rub / rublesToDollars) * 100)) + 0.0) / 100;
        return dollarsBill;
    }

    public static double dolToRubles(double dol) {
        System.out.print(" ₽ ");
        rublesBill = (((int) ((dollarsToRubles * dol) * 100)) + 0.0) / 100;
        return rublesBill;
    }

    public static int gatInstance () {
        return  instance;
    }

}
