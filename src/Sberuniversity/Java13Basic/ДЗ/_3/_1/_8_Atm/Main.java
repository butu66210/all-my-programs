package Sberuniversity.Java13Basic.ДЗ._3._1._8_Atm;

/*
8. Реализовать класс “банкомат” Atm.
Класс должен:
● Содержать конструктор, позволяющий задать курс валют перевода
долларов в рубли и курс валют перевода рублей в доллары (можно
выбрать и задать любые положительные значения)
● Содержать два публичных метода, которые позволяют переводить
переданную сумму рублей в доллары и долларов в рубли
● Хранить приватную переменную счетчик — количество созданных
инстансов класса Atm и публичный метод, возвращающий этот счетчик
(подсказка: реализуется через static)
 */



public class Main {
    public static void main(String[] args) {

        double dolToRcost = 104.31;
        double rubToDcost = 100.56;

        Atm.setMoneyCost(dolToRcost, rubToDcost);


        double rub = 104569.13;
        double dol = 18457.92;

        System.out.println(Atm.dolToRubles(dol));//Результат конвертации в рубли
        System.out.println(Atm.rubToDollars(rub));//Результат конвертации в доллары


        Atm[] newAtm = new Atm[100];

            for (int i = 0; i < 13; i++) {
                newAtm[i] = new Atm();
            }

        System.out.println("Инстантов: " + Atm.gatInstance());//Получаем количество инстантов
    }
}