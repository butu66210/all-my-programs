package Sberuniversity.Java13Basic.ДЗ._3._1._5_DayOfWeek;

public class DayOfWeek {

    private byte numberDayWeek;
    private String nameDayWeek;


    public void setNumberDayWeek(Byte newNumber) {
        numberDayWeek = newNumber;
    }


    public void setNameWeek (String newName) {
        nameDayWeek = newName;
    }

    public byte gatNumberDayWeek() {
        return numberDayWeek;
    }

    public String gatNameDayWeek () {
        return nameDayWeek;
    }


    public static void printWeek (DayOfWeek[] days) {
        for (int i = 0; i < days.length; i++) { //Выводим все значения
            System.out.println(days[i].gatNumberDayWeek() + " " + days[i].gatNameDayWeek());
    }
    }
}

