package Sberuniversity.Java13Basic.ДЗ._3._1._5_DayOfWeek;

/*

Необходимо реализовать класс DayOfWeek для хранения порядкового номера
дня недели (byte) и названия дня недели (String).
Затем в отдельном классе в методе main создать массив объектов DayOfWeek
длины 7. Заполнить его соответствующими значениями (от 1 Monday до 7
Sunday) и вывести значения массива объектов DayOfWeek на экран.
Пример вывода:
1 Monday
2 Tuesday
…
7 Sunday

 */

import Sberuniversity.Java13Basic.ДЗ._3._1._5_DayOfWeek.DayOfWeek;

public class Main {

    public static void main(String[] args) {

        DayOfWeek[] days = new DayOfWeek[7];

        days[0] = new DayOfWeek();
        days[0].setNumberDayWeek((byte) 1);
        days[0].setNameWeek("Понедельник");

        days[1] = new DayOfWeek();
        days[1].setNumberDayWeek((byte) 2);
        days[1].setNameWeek("Вторник");

        days[2] = new DayOfWeek();
        days[2].setNumberDayWeek((byte) 3);
        days[2].setNameWeek("Среда");

        days[3] = new DayOfWeek();
        days[3].setNumberDayWeek((byte) 4);
        days[3].setNameWeek("Четверг");

        days[4] = new DayOfWeek();
        days[4].setNumberDayWeek((byte) 5);
        days[4].setNameWeek("Пятница");

        days[5] = new DayOfWeek();
        days[5].setNumberDayWeek((byte) 6);
        days[5].setNameWeek("Суббота");

        days[6] = new DayOfWeek();
        days[6].setNumberDayWeek((byte) 7);
        days[6].setNameWeek("Воскресенье");

        DayOfWeek.printWeek(days);
    }
}
