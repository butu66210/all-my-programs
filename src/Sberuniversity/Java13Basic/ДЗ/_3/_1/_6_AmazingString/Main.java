package Sberuniversity.Java13Basic.ДЗ._3._1._6_AmazingString;

/*
Необходимо реализовать класс AmazingString, который хранит внутри себя
строку как массив char и предоставляет следующий функционал:
Конструкторы:
● Создание AmazingString, принимая на вход массив char
● Создание AmazingString, принимая на вход String
Публичные методы (названия методов, входные и выходные параметры
продумать самостоятельно). Все методы ниже нужно реализовать “руками”, т.е.
не прибегая к переводу массива char в String и без использования стандартных
методов класса String.
● Вернуть i-ый символ строки
● Вернуть длину строки
● Вывести строку на экран
● Проверить, есть ли переданная подстрока в AmazingString (на вход
подается массив char). Вернуть true, если найдена и false иначе
● Проверить, есть ли переданная подстрока в AmazingString (на вход
подается String). Вернуть true, если найдена и false иначе
● Удалить из строки AmazingString ведущие пробельные символы, если
они есть
● Развернуть строку (первый символ должен стать последним, а
последний первым и т.д.)
*/


public class Main {

    public static void main(String[] args) {

        System.out.println(AmazingString.substringSearch(AmazingString.gatCharArr()));
        //Ищем подстроку в дефолтном архиве
        System.out.println(AmazingString.gatCharArr()); //Вывели дефолтный архив
        System.out.println();

        char[] myCharArr = new char[]{'п', 'о', 'к', 'а'};//Создали новый массив Char
        AmazingString.setCharArr(myCharArr);//Ввели новый массив Char
        System.out.println(AmazingString.gatCharArr());//Показывает наличие подстроки
        //// и выводим массив
        System.out.println();

        String myString = "увидимся позже";//Создаем новую строку
        AmazingString.setStringToCharArr(myString);//Ввели новую строку
        System.out.println(AmazingString.gatCharArr());//Проверяем еще раз что стало
        System.out.println();

        AmazingString.removingSpaces(AmazingString.gatCharArr());//Удаляем пробелы
        System.out.println(AmazingString.substringSearch(AmazingString.gatCharArr()));
        //Ищем подстроку после удаления
        System.out.println(AmazingString.gatCharArr());//Проверяем еще раз что стало
        System.out.println();

        AmazingString.unwrapLine(AmazingString.gatCharArr());//Разворачиваем строку
        System.out.println(AmazingString.gatCharArr());//Проверяем еще раз что стало
    }
}
