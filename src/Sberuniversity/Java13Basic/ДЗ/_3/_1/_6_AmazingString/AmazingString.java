package Sberuniversity.Java13Basic.ДЗ._3._1._6_AmazingString;

public class AmazingString {

    private static char[] charArr = new char[]{'п', 'р', 'и', 'в', 'е', 'т'};//"Хранит в себе строку как массив Char"

    public static void setCharArr(char[] newCharArr) {//Создание charArr из массива char
        System.out.println(substringSearch(newCharArr));//Ищем подстроку и выводим результат
        charArr = newCharArr;
    }

    public static void setStringToCharArr(String newString) {//Создание charArr из String

        char[] cleanArr = new char[newString.length()];//Инициируем новый чистый массив
        //и присваиваем ему длину равную newString
        charArr = cleanArr;

        for (int i = 0; i < newString.length(); i++) {//Копируем строку в массив букв побуквенно
            charArr[i] = newString.charAt(i);
        }

        char[] substringSearchArr = charArr; //Ищем подстроку
        System.out.println(substringSearch(substringSearchArr));
    }

    public static char[] gatCharArr() {
        return charArr;
    }

    public static boolean substringSearch(char[] anyCharArr) {
        char whitespace = ' ';
        for (int i = 0; i < anyCharArr.length; i++) {
            if (charArr[i] == whitespace) {
                return true;
            }
        }
        return false;
    }

    public static void removingSpaces(char[] cArr) {
        char whitespace = ' ';//Что ищем?
        char[] newArr = new char[cArr.length];//Новый архив
        int x = 0;

        for (int i = 0; i < cArr.length; i++) {
            if (cArr[i] == whitespace) {
            } else {
                newArr[x] = cArr[i];
                x++;
            }
        }

        char[] newNewArr = new char[x];

        for (int i = 0; i < x; i++) {//Копируем в усеченный архив
            newNewArr[i] = newArr[i];
        }

        charArr = newNewArr;
    }

    public static void unwrapLine(char[] deployableArr) {

        char[] newArr = new char[deployableArr.length];//Cоздаем новый пустой массив
        int x = deployableArr.length;

        for (int i = 0; x > 0; i++) {//Копируем наоборот
            newArr[i] = deployableArr[x - 1];
            x--;
        }
        charArr = newArr;
    }
}
