package Sberuniversity.Java13Basic.ДЗ._3._1._2_Student;

/*

2. Необходимо реализовать класс Student.

У класса должны быть следующие приватные поля:
● String name — имя студента
● String surname — фамилия студента
● int[] grades — последние 10 оценок студента. Их может быть меньше, но
не может быть больше 10.

И следующие публичные методы:
● геттер/сеттер для name
● геттер/сеттер для surname
● геттер/сеттер для grades
● метод, добавляющий новую оценку в grades. Самая первая оценка
должна быть удалена, новая должна сохраниться в конце массива (т.е.
массив должен сдвинуться на 1 влево).
● метод, возвращающий средний балл студента (рассчитывается как
среднее арифметическое от всех оценок в массиве grades)

 */

public class Main {

    public static void main(String[] args) {

        Student student = new Student();

        student.setName("Юрий");//Вводим (устанавливаем) имя

        student.setSurname("Бутузов");//Вводим фамилию

        System.out.println(student.gatName());//Получаем имя

        System.out.println(student.getSurname());//Подучаем фамилию

        Integer[] studentGrades  = new Integer[]{97, 100, 99, 93, 98};//Создаем оценки

        student.setGrades(studentGrades); //Вводим оценки

        System.out.println("");

        student.getGrades(); //Получем оценки

        System.out.println("");

        student.inputNewGrade(100);//Вводим следующую оценку

        System.out.println("");

        student.getGrades(); //Получем оценки

        System.out.println("\n\nСредний балл: "+ student.averageRating());



    }
}
