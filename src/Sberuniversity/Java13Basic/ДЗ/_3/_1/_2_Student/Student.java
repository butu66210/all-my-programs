package Sberuniversity.Java13Basic.ДЗ._3._1._2_Student;


public class Student {

//    public Student student;
    private String name;            //Имя
    private String surname;     //Фамилия
    private int[] grades;           //Оценки (не больше 10)

//    public Student(String name) { //Создано для задания 3
//        this.name = name;
//    }

    //gat - получить.
    // Позволяет получать значение из закрытой переменной
    public String gatName() {   //Получаем имя
        return name;
    }

    //set - установка.
    // Позволяет сохранять в закрытые переменные новые значения
    public void setName(String newName) {   //Устанавливаем имя
        name = newName;
    }    //gat - получить.


    public String getSurname() {   //Получаем фамилию
        return surname;
    }


    public void setSurname(String newSurname) {   //Устанавливаем фамилию
        surname = newSurname;
    }

    public void getGrades() {   //Получаем оценки

        for (int i = 0; i < grades.length; i++) {
            System.out.print(grades[i] + " ");//Выводим массив для проверки
        }

    }


    public void setGrades(Integer[] newGrades) {//Устанавливаем оценки

        grades = new int[newGrades.length]; //Создаем массив конкретного размера

        if (newGrades.length < 10) {
            for (int i = 0; i < newGrades.length; i++) {  //Заполняем массив элементами списка
                grades[i] = newGrades[i];
            }
        }

        //ЕЩЕ ОДИН ВАРИАНТ РЕШЕНИЯ (МЕНЕЕ УДОБНЫЙ)
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Введите не более 10 оценок \nдля завершения операции введите любую букву:"); //\r \n - перенос на новую строку
//        //Объявляем список (у списков нет заданной длины)
//        ArrayList<Integer> stylist = new ArrayList<Integer>();
//        while (scanner.hasNextInt()) {  //Бесконечный сканер?
//            int i = scanner.nextInt(); //Вводим данные
//            stylist.add(i); //Присваиваем введенное следующему элементу массива?
//            //stylist.size - это размер (количество элементов) массива
//            //stylist.length - это длина строки массива? Но он не подешел
//            if (stylist.size() == 10) {
//                break;
//            }
//        }
//        grades = new int[stylist.size()]; //Указываем длину масссива
//        for (int i = 0; i < stylist.size(); i++) { //Заполняем массив элементами списка
//            grades[i] = stylist.get(i); //Так как stylist - это список, то прописывается так
//            }
    }

    public void inputNewGrade(int newGrade) {

        System.arraycopy(grades, 1, grades, 0, grades.length - 1);//Сдвигаем массив на 1 влево с помощью копирования

        grades[grades.length - 1] = newGrade;//Присваеваем последнему элементу новое значение

    }

    public int averageRating() {

        int average;


        int sum = 0;

        for (int i=0; i < grades.length; i++) {
            sum += grades[i];
        }

        average = sum/grades.length;

            return average;
    }
}




