package Sberuniversity.Java13Basic.ДЗ._3._1._1_Cat;

public class Cat {
    public static String catToDo;

    private static void Sleep() {
        catToDo = "Sleep";
        System.out.println(catToDo);
    }

    private static void Meow() {
        catToDo = "Meow";
        System.out.println(catToDo);
    }

    private static void Eat() {
        catToDo = "Eat";
        System.out.println(catToDo);
    }

    public void Status() {

        double randomMetod = (Math.random() * 3); //Рандомноче дабл-число до 3 не включительно
        System.out.println(randomMetod);


        switch ((int) randomMetod) { //Отсекаем точку и все после нее, начинаем сравнивать
            case 0 -> Cat.Sleep();
            case 1 -> Cat.Meow();
            case 2 -> Cat.Eat();
        }
    }
}
