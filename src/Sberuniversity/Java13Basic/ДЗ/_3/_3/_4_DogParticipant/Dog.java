package Sberuniversity.Java13Basic.ДЗ._3._3._4_DogParticipant;

public class Dog extends Participant {

    private String dogName;

    private double averageRating;//Средняя оценка
//    private int allPoints;//Сколько всего оценок
//    private int sumPoints;//Сумма оценок


    public void setAverageRating(int point, int nextpoint, int nextnextpoint) {
        this.averageRating = ((int)(((point + nextpoint + nextnextpoint) / 3.0)*10))/10.0;
    }

    public double getAverageRating() {
        return averageRating;
    }

    public String getDogName() {
        return dogName;
    }

    public void setDogName(String dogName) {
        this.dogName = dogName;
    }
}
