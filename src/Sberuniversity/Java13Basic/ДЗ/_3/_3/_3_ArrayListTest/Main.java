package Sberuniversity.Java13Basic.ДЗ._3._3._3_ArrayListTest;

/*

На вход передается N — количество столбцов в двумерном массиве и M —
количество строк. Необходимо вывести матрицу на экран, каждый элемент
которого состоит из суммы индекса столбца и строки этого же элемента. Решить
необходимо используя ArrayList.
Ограничения:
● 0 < N < 100
● 0 < M < 100

Входные данные
2 2

Выходные данные
0 1
1 2

Входные данные
3 5

Выходные данные
0 1 2
1 2 3
2 3 4
3 4 5
4 5 6

 */


import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        int N = scan.nextInt();//Столбцы
        int M = scan.nextInt();//Строки
        char nameList = 'A';


        ArrayList<ArrayList<Integer>> myIntList = new ArrayList<>(M);


        myIntList.add(0, new ArrayList<>());
//        for (int i = 0; i < M; i++) {
//            ArrayList<Integer> nameList = new ArrayList<>(N);
//        }

        for (int i = 0; i < M; i++) {
            myIntList.add(i, new ArrayList<>());
            for (int j = 0; j < N; j++) {
                myIntList.get(i).add(j, i+j);
            }
        }

        for (int i = 0; i < myIntList.size(); i++) {
            for (int j = 0; j < myIntList.get(i).size(); j++) {
                System.out.print(myIntList.get(i).get(j) + " ");
            }
            System.out.println();
        }
    }

}
