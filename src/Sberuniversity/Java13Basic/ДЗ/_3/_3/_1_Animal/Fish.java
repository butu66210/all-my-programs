package Sberuniversity.Java13Basic.ДЗ._3._3._1_Animal;

public class Fish extends Chordates{

    public String isFish() {
        return super.isChordate() +" У меня обтекаемая форма тела. Мое тело покрыто чешуей и склизким секретом. " +
                "У меня есть плавники и жабры. " +"\n";
    }

    public String characteristics () {
        setBeBorn("появляется из икринки. ");
        setWayToTravel("плавание. ");
        setMovementSpeed("низкая. ");
        return isFish() + "Способ рождения: " + getBeBorn() + "\n" + "Способ передвижения: "+ getWayToTravel() +"\n" +
                "Скорость передвижения: " + getMovementSpeed();
    }
}
