package Sberuniversity.Java13Basic.ДЗ._3._3._1_Animal;

public class Chiroptera extends Mammals {

    public static String isChiroptera() {
        return "У меня есть сильные перепончатые крылья " +
                "и я способен к длительному активному полету.";
    }

    public String aboutChiroptera() {
        return super.isMammals() + isChiroptera();
    }

    @Override
    public String characteristics() {
        setWayToTravel("полет. ");
        setMovementSpeed("высокая. ");
        return super.characteristics() + "\n"+ "Способ передвижения: "+ getWayToTravel() +"\n"+
                "Скорость передвижения: "+ getMovementSpeed() +"\n";
    }
}
