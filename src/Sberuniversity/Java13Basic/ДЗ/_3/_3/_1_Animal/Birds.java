package Sberuniversity.Java13Basic.ДЗ._3._3._1_Animal;

public class Birds extends Chordates{

    public String isFish() {
        return super.isChordate() +" Мое тело покрыто перьями, а передние конечности видоизменены. "+"\n";
    }

    public String characteristics () {
        setBeBorn("появляется из яйца. ");
        setWayToTravel("ходьба или полет. ");
        setMovementSpeed("низкая при ходьбе, высокая при полете. ");
        return isFish() + "Способ рождения: " + getBeBorn() + "\n" + "Способ передвижения: "+ getWayToTravel() +"\n" +
                "Скорость передвижения: " + getMovementSpeed();
    }


}
