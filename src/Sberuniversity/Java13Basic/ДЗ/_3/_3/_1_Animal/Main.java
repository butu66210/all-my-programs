package Sberuniversity.Java13Basic.ДЗ._3._3._1_Animal;

/*

Рассматриваются следующие животные:
● летучая мышь (Bat) ЖИВОТНЫЕ --> ХОРДОВЫЕ --> МЛЕКОПИТАЮЩИЕ --> РУКОКРЫЛЫЕ
● дельфин (Dolphin) ЖИВОТНЫЕ --> ХОРДОВЫЕ --> МЛЕКОПИТАЮЩИЕ --> КИТООБРАЗНЫЕ
● золотая рыбка (GoldFish): ЖИВОТНЫЕ --> ХОРДОВЫЕ --> РЫБЫ
● орел (Eagle) ЖИВОТНЫЕ --> ХОРДОВЫЕ --> ПТИЦЫ
Все животные одинаково едят и спят (предположим), и никто из животных не
должен иметь возможности делать это иначе.
Еще животные умеют по-разному рождаться (wayOfBirth):
● Млекопитающие (Mammal) живородящие.
● Рыбы (Fish) мечут икру.
● Птицы (Bird) откладывают яйца.
Помимо этого бывают некоторые особенности, касающиеся передвижения.
Бывают летающие животные (Flying) и плавающие (Swimming). Однако орел
летает быстро, а летучая мышь медленно. Дельфин плавает быстро, а золотая
рыбка медленно.

Согласно этим утверждениям нужно создать иерархию, состоящую из классов,
абстрактных классов и/или интерфейсов. Каждое действие или утверждение
подразумевает под собой вызов void метода, в котором реализован вывод на
экран описания текущего действия.


*/


public class Main {

    public static void main(String[] args) {
        Chiroptera batMasha = new Chiroptera();
        System.out.println("Летучая мышь.");
        System.out.println(batMasha.aboutChiroptera());
        System.out.println(batMasha.characteristics() + "\n");

        Cetaceans dolphin = new Cetaceans();
        System.out.println("Дельфин.");
        System.out.println(dolphin.aboutCetaceans());
        System.out.println(dolphin.characteristics() + "\n");

        Fish goldFish = new Fish();
        System.out.println("Золотая рыбка.");
        System.out.println(goldFish.characteristics() + "\n");

        Birds eagle = new Birds();
        System.out.println("Орел.");
        System.out.println(eagle.characteristics() + "\n");
    }
}
