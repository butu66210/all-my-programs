package Sberuniversity.Java13Basic.ДЗ._3._3._1_Animal;

public class Cetaceans extends Mammals{

    public static String isCetaceans() {
        return "Я хорошо приспособлен к водной среде " +
                "и никогда ее не покидаю.";
    }

    public String aboutCetaceans() {
        return super.isMammals() + isCetaceans();
    }

    @Override
    public String characteristics() {
        setWayToTravel("плавание. ");
        setMovementSpeed("высокая. ");
        return super.characteristics() + "\n"+ "Способ передвижения: " + getWayToTravel()+ "\n" +
                "Скорость передвижения: " + getMovementSpeed() +"\n";
    }
}
