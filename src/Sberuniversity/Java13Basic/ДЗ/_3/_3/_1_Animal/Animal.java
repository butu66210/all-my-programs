package Sberuniversity.Java13Basic.ДЗ._3._3._1_Animal;

public class Animal {

    public String isAnimal() {
        return "Я - животное.";
    }

    private static String eat = "Как не в себя"; //Все едят

    private static String sleep = "Иногда храплю"; //Все спят

    private String beBorn; //Как рождаются?

    public String getBeBorn() {
        return beBorn;
    }

    public void setBeBorn(String beBorn) {
        this.beBorn = beBorn;
    }

    private String wayToTravel; //Способ передвижения

    public String getWayToTravel() {
        return wayToTravel;
    }

    public void setWayToTravel(String wayToTravel) {
        this.wayToTravel = wayToTravel;
    }

    private String movementSpeed; //Скорость передвижения

    public String getMovementSpeed() {
        return movementSpeed;
    }

    public void setMovementSpeed(String movementSpeed) {
        this.movementSpeed = movementSpeed;
    }
}
