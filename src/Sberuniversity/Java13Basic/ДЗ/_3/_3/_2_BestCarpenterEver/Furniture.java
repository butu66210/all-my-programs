package Sberuniversity.Java13Basic.ДЗ._3._3._2_BestCarpenterEver;

public class Furniture {

    private int length; //длина
    private int width; //ширина
    private int height;//высота

    public Furniture (int length, int width, int height) {
        this.length = length;
        this.width = width;
        this.height = height;
//        setLength(length);
//        setWidth(width);
//        setHeight(height);
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}


