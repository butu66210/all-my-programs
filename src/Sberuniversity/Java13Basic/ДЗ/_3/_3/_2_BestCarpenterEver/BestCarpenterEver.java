package Sberuniversity.Java13Basic.ДЗ._3._3._2_BestCarpenterEver;

/*

2. Цех по ремонту BestCarpenterEver умеет чинить некоторую Мебель. К
сожалению, из Мебели он умеет чинить только Табуретки, а Столы, например,
нет. Реализовать метод в цеху, позволяющий по переданной мебели
определять, сможет ли ей починить или нет. Возвращать результат типа
boolean. Протестировать метод.

 */



public class BestCarpenterEver {

    private int downPossibleLength = 100;
    private int upPossibleLength = 300;

    private int downPossibleWidth = 50;
    private int upPossibleWidth = 140;

    private int downPossibleHeight = 80;
    private int upPossibleHeight = 220;
    public BestCarpenterEver (Furniture newFurniture) {

        boolean isPossible = false;

        int a = newFurniture.getLength(); //длина
        int b = newFurniture.getWidth();//ширина
        int c = newFurniture.getHeight();//высота

        int adl = downPossibleLength;
        int aul = upPossibleLength;

        int bdw = downPossibleWidth;
        int buw = upPossibleWidth;

        int cdw = downPossibleHeight;
        int cuw = upPossibleHeight;

        if (a >= adl & a<= aul) {
            if (b >= bdw & b <= buw) {
                if (c >= cdw & c<= cuw) {
                    isPossible = true;
                }
            }
        }

        System.out.println("Возможность создания данного предмета медели: " + isPossible);

    }
}
