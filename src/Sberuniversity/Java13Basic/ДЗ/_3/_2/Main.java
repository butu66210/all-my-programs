package Sberuniversity.Java13Basic.ДЗ._3._2;





public class Main {

    public static void main(String[] args) {

        Library.setLibrary(5);//Указываем количество книг в библиотеке

        Library.setBook("Все, что я думаю об обучении программированию",
                "Юрий Бутузов");//Вносим книгу

//        Library.getLibrary(); //Просматриваем библиотеку

        Library.setBook("Особый путь француза",
                "Наполион Бонапарт");

//        Library.getLibrary();

        Library.setBook("Особый путь француза",
                "Наполион Бонапарт");//Пытаемся внести имеющуюся книгу

        Library.setBook("В прошлой жизни я был рыбой",
                "Чарли Чаплин");

//        Library.getLibrary();

        Library.setBook("Как придумать гениальный псевдоним и не иметь проблем с запрещенкой",
                "Мерлин Монро");//Вносим книгу

        Library.setBook("Почему от длительного сидения отваливается спина",
                "Юрий Бутузов");//Вносим книгу

        Library.setVisitors(5);//Количество читателей

        Library.setVisitorToLibrary("Федя");
        Library.setVisitorToLibrary("Маша");
        Library.setVisitorToLibrary("Лена");
        Library.setVisitorToLibrary("Дима");
        Library.setVisitorToLibrary("Дедушка Мороз");

        Library.getVisitors();//Просматриваем читателей

        System.out.println(Library.getBookStatus(2));//Взяли книгу или нет?

        Library.getLibrary();

        Library.searchAndDeleteBook("Как придумать гениальный псевдоним и не иметь проблем с запрещенкой");//Удаляем книгу и делаем размер архива книг на 1 меньше

        Library.getLibrary();

        Library.searchBook("Особый путь француза");

        Library.searchAutosBooks("Юрий Бутузов");//Ищем книги по автору

        Library.takeBookToReed("Маша", "В прошлой жизни я был рыбой");//Маша берет книгу

        Library.takeBookToReed("Федя", "В прошлой жизни я был рыбой");//Федя пробует взять эту же книгу

        Library.giveBookToLibrary("Лена", "В прошлой жизни я был рыбой");//Лена хочет помочь сдать книгу

        Library.giveBookToLibrary("Маша", "В прошлой жизни я был рыбой");//Лена хочет помочь сдать книгу

        Library.takeBookToReed("Федя", "В прошлой жизни я был рыбой");//Федя снова пробует взять эту книгу

        Library.giveBookToLibrary("Федя", "В прошлой жизни я был рыбой");//Возвращает книгу

        Library.getSrednOsenka("В прошлой жизни я был рыбой");//Возвращаем среднюю оценку

        //ВСЕ! СПАСИБО ЗА ВНИМАНИЕ!


    }

}
