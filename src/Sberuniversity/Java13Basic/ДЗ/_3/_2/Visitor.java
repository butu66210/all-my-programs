package Sberuniversity.Java13Basic.ДЗ._3._2;

public class Visitor {

    private String name;

    private String surname;

    private String identifier;//Впервые ли берет книгу

    private String newIdentifier;//Есть ли сейчас книга на руках



    public void satVisitor(String newName) {
        name = newName;
    }

    public void satVisitorsIndentifier(String newI) {
        identifier = newI;
    }

    public void satVisitorsNewIndentifier(String newI) {
        newIdentifier = newI;
    }

    public String getVisitorsName() {
        return name;
    }


    public String getVisitorsIndentifier() {
        return identifier;
    }

    public String getVisitorsNewIndentifier() {
        return newIdentifier;
    }


    //Имя посетителя
    //Идентификатор null до тех пор пока не взял книгу
    //Идентификатор есть ли на руках книг


}
