package Sberuniversity.Java13Basic.ДЗ._3._2;

public class Book {

/* Название
    Автор
*/

    //Статус книги: одолжена или нет

    private String name;

    private String author;

    private String status;



    private double srednyaOcenka;
    private int summaOtsenok;
    private int kolichestvoOtsenok;

    public void satOcenku (int ocenka) {
        summaOtsenok += ocenka;
        kolichestvoOtsenok++;
        srednyaOcenka = (summaOtsenok + 0.0) / (kolichestvoOtsenok +0.0);
    }

    public Double getSrednyaOcenka () {
        return srednyaOcenka;
    }




    public void satBook (String newName, String newAuthor) {
        name = newName;
        author = newAuthor;
    }

    public String getBookName () {
        return name;
    }

    public String getBookAuthor() {
        return author;
    }

    public String getBookStatus() {
        return status;
    }

    public void setBookStatus(String chitatel) {
        status = chitatel;
    }
}
