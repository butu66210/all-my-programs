package Sberuniversity.Java13Basic.ДЗ._3._2;

import java.util.Objects;
import java.util.Scanner;

public class Library {

    private static Book[] library = new Book[1];
    private static Visitor[] arrVisitors = new Visitor[1];
    private static int booksInLibrary = 0;
    private static int visitorsInLibrary;

//    public void createLibrary (int amount) {
//
//    }

    public static int getBookseInLibrary() {
        return booksInLibrary;
    }//Выводим количество книг в библиотеке
//    public static int getBookseInLibrary() {
//        return booksInLibrary;
//    }

    public static String getBookStatus(int numberOfBook) {//Выводим статус книги
        return library[numberOfBook].getBookStatus();
    }


    public static void setLibrary(Integer x) {//Создаем библиотеку
        library = new Book[x];
        for (int i = 0; i < x; i++) {
            library[i] = new Book();
        }
    }

    public static void setVisitors(Integer x) {//Создаем посетителей
        arrVisitors = new Visitor[x];
        for (int i = 0; i < x; i++) {
            arrVisitors[i] = new Visitor();
        }
    }

    public static void getLibrary() {//Выводим библиотеку
        for (int i = 0; i < booksInLibrary; i++) {
            System.out.println(library[i].getBookName() + "\n" + library[i].getBookAuthor() + "\n");
        }
    }

    public static void getVisitors() { // Выводим посетителей
        System.out.println("Наши читатели: \n");
        for (int i = 0; i < visitorsInLibrary; i++) {
            System.out.println(arrVisitors[i].getVisitorsName());
        }
        System.out.println("");
    }

//    public void setBooksToLibrary(Book[] library) {
//        Library.library = library;
//    }

    public static void setBookToLibrary(String newName, String newAuthor) { //Добавляем книгу
        library[getBookseInLibrary()].satBook(newName, newAuthor);
        booksInLibrary++;
    }

    public static void setVisitorToLibrary(String newName) {//Добавляем читателя
        arrVisitors[visitorsInLibrary].satVisitor(newName);
        visitorsInLibrary++;
    }


    public static void setBook(String newName, String newAuthor) {//Добавляем 1 книгу
        Book[] books = new Book[1];
        books[0] = new Book();
        books[0].satBook(newName, newAuthor);
        String s = books[0].getBookName();
        int z = 0;
        for (int i = 0; i < library.length; i++) {
            String ss = library[i].getBookName();
            if (Objects.equals(s, ss)) {
                z++;
            }
        }
        if (z != 0) {
            System.out.println("\n !!! Ошибка добавления! Введите уникальную книгу !!! \n");
        } else {
            Library.setBookToLibrary(newName, newAuthor);
            System.out.println("\n !!! Книга добавлена в библиотеку !!! \n");
        }
    }

    public static void searchAndDeleteBook(String bookName) { //Удаляем книгу по названию
        String s = bookName; //введенное имя
        Book[] newlibrary = new Book[booksInLibrary - 1];//новый архив
        int x = 0;//счетчик несовпадений
        int y = 0;//счетчик совпадений
        for (int i = 0; i < booksInLibrary; i++) {
            String ss = library[i].getBookName();//строка для поиска по всему архиву
            if (Objects.equals(s, ss)) {//если введенная страка и порядковая совпадают
                y = i;//счетчик
            } else {//если не совпадают
                newlibrary[x] = new Book();//копируем элемент нового массива то, что без совпадения
                newlibrary[x] = library[i];
                x++;
            }
        }
        if (y > 0 & library[y].getBookStatus() != null) {
            library = newlibrary;//Массив главный становится таким же как новый без совпадения
            booksInLibrary--;
            System.out.println("\n !!! Книга удалена. Номера книг изменены !!! \n");
        } else {
            System.out.println("\n!!! Данная книга не найдена в библиотеке !!!\n");
        }
    }

    public static void searchBook(String nameForSearch) {//Ищем книгу по названию

        String s = nameForSearch;
        for (int i = 0; i < library.length; i++) {
            String ss = library[i].getBookName();
            if (Objects.equals(s, ss)) {
                System.out.println("\n !!! Книга найдена !!! \n" + library[i].getBookAuthor() + "\n" + library[i].getBookName());
            }
        }
    }

    public static void searchAutosBooks(String autorForSearch) {//Ищем книгу по автору

        String s = autorForSearch;
        for (int i = 0; i < library.length; i++) {
            String ss = library[i].getBookAuthor();
            if (Objects.equals(s, ss)) {
                System.out.println("\n" + library[i].getBookAuthor() + "\n" + library[i].getBookName() + "\n");
            }
        }
    }

    public static void takeBookToReed(String visitor, String bookName) {//Берем книгу из библиотеки

        int x = -1;
        String v = visitor;
        for (int i = 0; i < library.length; i++) {
            String vv = arrVisitors[i].getVisitorsName();
            if (Objects.equals(v, vv)) {
                x = i;
                break;
            }
        }


        int h = -1;
        if (x >= 0) {
            String s = bookName;
            for (int i = 0; i < library.length; i++) {
                String ss = library[i].getBookName();
                if (Objects.equals(s, ss)) {
                    h = i;
                    break;
                }
            }


            if (h >= 0 & library[h].getBookStatus() == null & arrVisitors[x].getVisitorsNewIndentifier() == null & arrVisitors[x].getVisitorsIndentifier() == null) {

                arrVisitors[x].satVisitorsIndentifier("Билет выдан");
                System.out.println("\n ВАМИ ПОЛУЧЕН ЧИТАТЕЛЬСИКИЙ БИЛЕТ \n");

                arrVisitors[x].satVisitorsNewIndentifier("КНИГА НА РУКАХ");
                library[h].setBookStatus(visitor);
                System.out.println("\n КНИГА ВЗЯТА \n");
                System.out.println("\n" + library[h].getBookAuthor() + "\n" + library[h].getBookName() + "\n");

            } else if (h >= 0 & library[h].getBookStatus() == null & arrVisitors[x].getVisitorsNewIndentifier() == null) {
                arrVisitors[x].satVisitorsNewIndentifier("КНИГА НА РУКАХ");
                library[h].setBookStatus(visitor);
                System.out.println("\n КНИГА ВЗЯТА \n");
                System.out.println("\n" + library[h].getBookAuthor() + "\n" + library[h].getBookName() + "\n");
            } else {
                System.out.println("\n !!! Неверное название книги (такое же оповещение приходит, когда на руках уже есть книга или она не свободна)!!! \n");
            }
        }
    }

    public static <scaner> void giveBookToLibrary(String visitor, String bookName) {//Сдаем книгу в библиотеку

        int x = -1;//Находим читателя
        String v = visitor;
        for (int i = 0; i < library.length; i++) {
            String vv = arrVisitors[i].getVisitorsName();
            if (Objects.equals(v, vv)) {
                x = i;
                break;
            }
        }


        int h = -1;//Находим книгу
        if (x >= 0) {
            String s = bookName;
            for (int i = 0; i < library.length; i++) {
                String ss = library[i].getBookName();
                if (Objects.equals(s, ss)) {
                    h = i;
                    break;
                }
            }


            if (h >= 0 & x >= 0 & library[h].getBookStatus() == visitor) {

                arrVisitors[x].satVisitorsNewIndentifier(null);
                library[h].setBookStatus(null);
                System.out.println("\n КНИГА СДАНА \n");

                Scanner scan = new Scanner(System.in);
                System.out.println("\n!!! Пожалуйста, оцените книгу от 1 до 10 !!!\n");
                int o = scan.nextInt();
                library[h].satOcenku(o);

            } else if (h >= 0 & x >= 0 & library[h].getBookStatus() != visitor) {
                System.out.println("\n!!! Книгу может сдать только читатель !!!\n");

            } else {
                System.out.println("\n !!! Неверное название книги или имя читателя!!! \n");
            }
        }
    }

    public static void getSrednOsenka (String nameOfBook) {

        int h = -1;//Находим книгу
            String s = nameOfBook;
            for (int i = 0; i < library.length; i++) {
                String ss = library[i].getBookName();
                if (Objects.equals(s, ss)) {
                    h = i;
                    break;
                }
            }
        if (h >= 0) {
            System.out.println(library[h].getSrednyaOcenka());
        }
    }
}


//Недоделанный код для добавить несколько книг из архива:
//    public void setBook (Integer amount) {//Указываем количество
//        Book[] books = new Book[amount];
//        books[0].getBookName();
//        books[0] = new Book();
//        books[0].satBook("Старинная чушь", "Марк Аврелий");
//        books[1] = new Book();
//        books[1].satBook("Старинная чушь", "Марк Аврелий");
//        books[0].getBookName();
//    }


    /*
        Работа со списком существующих книг в библиотеке. Здесь все добавленные книги.

        1. Добавить новую книгу в библиотеку, если книги с таким наименованием ещё нет
        в библиотеке. Если книга в настоящий момент одолжена, то считается, что она
        всё равно есть в библиотеке (просто в настоящий момент недоступна). //CДЕЛАНО

        2. Удалить книгу из библиотеки по названию, если такая книга в принципе есть в
        библиотеке и она в настоящий момент не одолжена.//СДЕЛАНО

        3. Найти и вернуть книгу по названию.//СДЕЛАНО

        4. Найти и вернуть список книг по автору.//СДЕЛАНО

        Механизм одалживания книги посетителю. Каждый посетитель в один момент времени
        может читать только одну книгу.//СДЕЛАНО

        5. Одолжить книгу посетителю по названию, если выполнены все условия:
        a. Она есть в библиотеке.
        b. У посетителя сейчас нет книги.
        c. Она не одолжена.
        Также если посетитель в первый раз обращается за книгой — дополнительно
        выдать ему идентификатор читателя.//СДЕЛАНО

        6. Вернуть книгу в библиотеку от посетителя, который ранее одалживал книгу. Не
        принимать книгу от другого посетителя.
        a. Книга перестает считаться одолженной.
        b. У посетителя не остается книги.//СДЕЛАНО

        Тестирование.
        7. В методе main написать несколько тестов на реализованный функционал.//СДЕЛАНО

        Дополнительная задача*:
        8. Добавить функционал оценивания книг посетителем при возвращении в
        библиотеку. Оценка книги рассчитывается как среднее арифметическое оценок
        всех посетителей, кто брал эту книгу. Реализовать метод, возвращающий
        оценку книги по её наименованию.//СДЕЛАНО
             */

